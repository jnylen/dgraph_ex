defmodule DgraphEx do
  @moduledoc false

  alias DgraphEx.Core.{Alter, Delete, Expr, Field, Set, Query, Vertex}
  alias DgraphEx.Util

  require Expr.Math
  require Vertex

  Vertex.query_model()
  use Expr
  use Field
  # use Schema

  # use Mutation
  use Alter
  use Delete
  use Set

  use Query

  defmacro math(block) do
    quote do: Expr.Math.math(unquote(block))
  end

  def into_model(resp, model) do
    models =
      model.__vertex__(:fields)
      |> Enum.reject(&is_nil(&1.models))
      |> Enum.map(fn field ->
        {field.predicate, field.models}
      end)

    resp
    |> do_into_model(model, models)
    |> do_into_model(model.__struct__)
  end

  defp do_into_model(%{} = resp, _model, items) when is_list(items) do
    resp
    |> Map.merge(
      Enum.reduce(items, %{}, fn {pred, models}, acc ->
        acc
        |> Map.put(
          pred,
          do_into_model(Map.get(resp, pred), calculate_type(Map.get(resp, pred), models))
        )
      end)
    )
  end

  defp do_into_model(nil, _), do: nil
  defp do_into_model(%{} = item, %{} = model), do: Vertex.populate_model(model, item)
  defp do_into_model(item, nil), do: item

  defp calculate_type(%{_type_: type} = _predmodel, models) do
    models
    |> List.keyfind(String.to_existing_atom(type), 0)
    |> case do
      {_, model} -> model.__struct__
      _ -> nil
    end
  end

  defp calculate_type(_, _), do: nil

  ########## OLD
  @spec into({:error, any()} | {:ok, map()} | map(), any(), any()) ::
          {:error, any()} | %{optional(atom()) => nil | [any()] | %{__struct__: any()}}
  def into({:error, _} = err, _, _), do: err

  def into({:ok, resp}, module, key)
      when is_atom(key) and is_map(resp),
      do: into(resp, module, key)

  def into(resp, module, key) when is_map(resp) do
    resp
    |> Util.get_value(key, {:error, {:invalid_key, key}})
    |> do_into(module, key)
  end

  defp do_into(%{} = item, %{} = model), do: Vertex.populate_model(model, item)
  defp do_into({:error, _} = err, _, _), do: err

  defp do_into(items, module, key) when is_atom(module) do
    do_into(items, module.__struct__, key)
  end

  defp do_into(items, %{} = model, key) when is_list(items) do
    %{key => Enum.map(items, fn item -> do_into(item, model) end)}
  end

  defp do_into(%{} = item, %{} = model, key), do: %{key => do_into(item, model)}
end
